**Desarrollo de aplicaciones web**

**Bejarano Félix Bernardo Daniel**

**5AVP**

Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: d6220eb967ff33a3542c9adb25e3440acda85a0f

Practica #2 - 09/09/2022 - Práctica JavaScript 
Commit: 1698fc1cc0e913fc7607ab01754ab1df855d70be

Practica #3 - 15/09/2022 - Práctica web con base de datos - parte 1
Commit: 960da244e45f140cf01991e5a3b9506873ebe70a

Practica #4 - Práctica web con base de datos - Vista de consulta de datos
Commit: 6b4dbf5d8a81f64f6da31412d2ea7fd885a30a30

Práctica #5 - 22/09/2022 - Práctica web con base de datos - Vista de registro de datos
Commit: 8c0a73daeeca119518b7635c1db82b48360c3704

Práctica #6 - 26/09/2022 - Práctica web con base de datos - Conexión con base de datos
Commit: 6c48203ae905f8164a0188351bcb67f9bba2f1fb

